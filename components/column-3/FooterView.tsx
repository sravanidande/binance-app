import { Box, Flex, Link, Tab, TabList, Tabs, Text } from '@chakra-ui/react'
import React from 'react'

export const FooterView: React.FC = () => {
  return (
    <Box bg="#171923" mt={1}>
      <Tabs overflowX="hidden">
        <TabList>
          <Tab
            _focus={{ outline: 'none' }}
            fontWeight="bold"
            fontSize="sm"
            color="#606060"
            _selected={{ color: 'white', borderBottom: '2px solid teal' }}
          >
            Open Orders(0)
          </Tab>
          <Tab
            _focus={{ outline: 'none' }}
            fontWeight="bold"
            fontSize="sm"
            color="#606060"
            _selected={{ color: 'white', borderBottom: '2px solid teal' }}
          >
            Order History
          </Tab>
          <Tab
            _focus={{ outline: 'none' }}
            fontWeight="bold"
            fontSize="sm"
            color="#606060"
            _selected={{ color: 'white', borderBottom: '2px solid teal' }}
          >
            Trade History
          </Tab>
          <Tab
            _focus={{ outline: 'none' }}
            fontWeight="bold"
            fontSize="sm"
            color="#606060"
            _selected={{ color: 'white', borderBottom: '2px solid teal' }}
          >
            Funds
          </Tab>
        </TabList>
      </Tabs>
      <Flex alignItems="center" justifyContent="center" h="100px">
        <Text>
          <Link color="yellow.300">Log In</Link> or{' '}
          <Link color="yellow.300">Register Now</Link> to trade
        </Text>
      </Flex>
    </Box>
  )
}
