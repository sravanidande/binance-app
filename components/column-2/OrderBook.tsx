import {
  HStack,
  Icon,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
} from '@chakra-ui/react'
import React from 'react'
import {
  AiOutlineCaretDown,
  AiOutlineMenuFold,
  AiOutlineMenuUnfold,
  AiOutlinePicLeft,
} from 'react-icons/ai'
import { TabTable1 } from './TabTable1'
import { TabTable2 } from './TabTable2'
import { TabTable3 } from './TabTable3'

export const MenuRate: React.FC = () => {
  return (
    <Menu>
      <MenuButton>
        <HStack>
          <Text fontSize="sm"> 0.01</Text> <Icon as={AiOutlineCaretDown} />
        </HStack>
      </MenuButton>
      <MenuList>
        <MenuItem>0.01</MenuItem>
        <MenuItem>0.1</MenuItem>
        <MenuItem>1</MenuItem>
        <MenuItem>10</MenuItem>
        <MenuItem>50</MenuItem>
      </MenuList>
    </Menu>
  )
}

export const OrderBook: React.FC = () => {
  return (
    <Tabs size="md" variant="unstyled">
      <TabList>
        <HStack spacing={10} align="stretch">
          <Tab
            _selected={{ bg: 'teal.900' }}
            _focus={{
              outline: 'none',
              bg: 'transparent',
              bgColor: 'transparent',
            }}
          >
            <Icon as={AiOutlineMenuFold} />
          </Tab>
          <Tab _selected={{ bg: 'teal.900' }} _focus={{ outline: 'none' }}>
            <Icon as={AiOutlineMenuUnfold} />
          </Tab>
          <Tab _selected={{ bg: 'teal.900' }} _focus={{ outline: 'none' }}>
            <Icon as={AiOutlinePicLeft} />
          </Tab>
          <MenuRate />
        </HStack>
      </TabList>

      <TabPanels>
        <TabPanel>
          <TabTable1 />
        </TabPanel>
        <TabPanel>
          <TabTable2 />
        </TabPanel>
        <TabPanel>
          <TabTable3 />
        </TabPanel>
      </TabPanels>
    </Tabs>
  )
}
