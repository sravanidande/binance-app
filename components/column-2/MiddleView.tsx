import { Tabs, TabList, Tab, TabPanels, TabPanel, Box } from '@chakra-ui/react'
import React from 'react'
import { OrderBook } from './OrderBook'
import { Trades } from './Trades'

export const MiddleView: React.FC = () => {
  return (
    <Box mt={1} bg="#171923">
      <Tabs>
        <TabList>
          <Tab
            _focus={{ outline: 'none' }}
            fontWeight="bold"
            fontSize="sm"
            color="#606060"
            _selected={{ color: 'white', borderBottom: '2px solid teal' }}
          >
            Chart
          </Tab>
          <Tab
            _focus={{ outline: 'none' }}
            fontWeight="bold"
            fontSize="sm"
            color="#606060"
            _selected={{ color: 'white', borderBottom: '2px solid teal' }}
          >
            Order Book
          </Tab>
          <Tab
            _focus={{ outline: 'none' }}
            fontWeight="bold"
            fontSize="sm"
            color="#606060"
            _selected={{ color: 'white', borderBottom: '2px solid teal' }}
          >
            Trades
          </Tab>
        </TabList>

        <TabPanels>
          <TabPanel>
            <p>one!</p>
          </TabPanel>
          <TabPanel>
            <OrderBook />
          </TabPanel>
          <TabPanel>
            <Trades />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Box>
  )
}
