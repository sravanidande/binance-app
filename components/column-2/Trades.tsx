import { Table, Tbody, Td, Th, Thead, Tr } from '@chakra-ui/react'
import { getHours, getMinutes, getSeconds, getTime } from 'date-fns'
import React from 'react'

// const oneUnit = { price: 672.15, amount: 0.6546 }
const units = [
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
]

export const Trades: React.FC = () => {
  const time = React.useMemo(() => getTime(new Date()), [])

  return (
    <Table variant="unstyled" size="sm">
      <Thead fontSize="sm">
        <Tr>
          <Th isNumeric fontSize="xs" color="#606060" letterSpacing={0}>
            Price(BUSD)
          </Th>
          <Th isNumeric fontSize="xs" color="#606060" letterSpacing={0}>
            Amount(BNB)
          </Th>
          <Th isNumeric fontSize="xs" color="#606060" letterSpacing={0}>
            Time
          </Th>
        </Tr>
      </Thead>
      <Tbody>
        {units.map((unit, index) => (
          <Tr key={index}>
            <Td fontSize="xs" color="teal" fontWeight="semibold">
              {unit.price}
            </Td>
            <Td fontSize="xs" color="white">
              {unit.amount}
            </Td>
            <Td fontSize="xs" color="white">{`${getHours(time)}:${getMinutes(
              time,
            )}:${getSeconds(time)}`}</Td>
          </Tr>
        ))}
      </Tbody>
    </Table>
  )
}
