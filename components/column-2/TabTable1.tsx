import { Table, Tbody, Td, Th, Thead, Tr } from '@chakra-ui/react'
import React from 'react'

const units = [
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
  { price: 672.15, amount: 0.6546 },
]

export const TabTable1: React.FC = () => {
  return (
    <Table variant="unstyled" size="sm">
      <Thead fontSize="sm">
        <Tr>
          <Th isNumeric fontSize="xs" color="#606060" letterSpacing={0}>
            Amount
          </Th>
          <Th isNumeric fontSize="xs" color="#606060" letterSpacing={0}>
            Price
          </Th>
          <Th isNumeric fontSize="xs" color="#606060" letterSpacing={0}>
            Price
          </Th>
          <Th isNumeric fontSize="xs" color="#606060" letterSpacing={0}>
            Amount
          </Th>
        </Tr>
      </Thead>
      <Tbody>
        {units.map((unit, index) => (
          <Tr key={index}>
            <Td fontSize="xs" color="white" fontWeight="semibold">
              {unit.price}
            </Td>
            <Td fontSize="xs" color="green.400">
              {unit.amount}
            </Td>
            <Td fontSize="xs" color="red.500" fontWeight="semibold">
              {unit.price}
            </Td>
            <Td fontSize="xs" color="white">
              {unit.amount}
            </Td>
          </Tr>
        ))}
      </Tbody>
    </Table>
  )
}
