import { Grid } from '@chakra-ui/layout'
import { Box } from '@chakra-ui/react'
import React from 'react'

export const BinanceLayout: React.FC = () => (
  <Grid
    h="100vh"
    gridTemplateColumns={{
      base: '1fr 1fr',
      md: '1fr 3fr 2fr',
    }}
    gridTemplateRows={{
      base: 'auto',
      md: '0.5fr 2fr 2fr 1fr',
    }}
    gridTemplateAreas={{
      base: `'header header' 'leftcard rightcard' 'map map' 'leftscrollcard rightscrollcard' 'footer footer'`,
      md: `'header header rightcard' 'leftcard map rightcard' 'leftscrollcard map rightscrollcard' 'footer footer footer'`,
    }}
    gap={1}
  >
    <Box gridArea="header" bg="gray.500">
      header
    </Box>
    <Box gridArea="leftcard" bg="green.500">
      leftcard
    </Box>
    <Box gridArea="rightcard" bg="red.500">
      rightcard
    </Box>
    <Box gridArea="leftscrollcard" bg="yellow.500">
      leftscrollcard
    </Box>
    <Box gridArea="rightscrollcard" bg="orange.500">
      rightscrollcard
    </Box>
    <Box gridArea="map" bg="blue.500">
      map
    </Box>
    <Box gridArea="footer" bg="gray.500">
      footer
    </Box>
  </Grid>
)
