/* eslint-disable arrow-body-style */
import { Tab, TabList, Tabs } from '@chakra-ui/react'
import React from 'react'

export const TabView: React.FC = () => {
  return (
    <Tabs size="sm" variant="unstyled">
      <TabList bg="teal.900">
        <Tab
          _selected={{
            bg: 'teal',
          }}
          _focus={{ outline: 'none' }}
        >
          One
        </Tab>
        <Tab _selected={{ bg: 'teal' }} _focus={{ outline: 'none' }}>
          Two
        </Tab>
        <Tab _selected={{ bg: 'teal' }} _focus={{ outline: 'none' }}>
          Three
        </Tab>
      </TabList>
    </Tabs>
  )
}
