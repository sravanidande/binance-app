import { Box, Heading, SimpleGrid, Text } from '@chakra-ui/layout'
import React from 'react'

export const TopLeft: React.FC = () => {
  return (
    <SimpleGrid columns={2} gap={3}>
      <Box mt={4}>
        <Text fontSize="xs" color="#606060">
          24h High
        </Text>
        <Heading m={0} fontSize="xs">
          644.68
        </Heading>
      </Box>
      <Box mt={4}>
        <Text fontSize="xs" color="#606060">
          24h Low
        </Text>
        <Heading m={0} fontSize="xs">
          614.25
        </Heading>
      </Box>
      <Box>
        <Text fontSize="xs" color="#606060">
          24h Volume
        </Text>
        <Text fontSize="xs" color="#606060">
          (BNB)
        </Text>
        <Heading m={0} fontSize="xs">
          512,856.57
        </Heading>
      </Box>
      <Box>
        <Text fontSize="xs" color="#606060">
          24h Volume
        </Text>
        <Text fontSize="xs" color="#606060">
          (BUSD)
        </Text>
        <Heading m={0} fontSize="xs">
          323.94M
        </Heading>
      </Box>
    </SimpleGrid>
  )
}
