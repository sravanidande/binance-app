import { Flex, Heading, HStack, Text } from '@chakra-ui/layout'
import React from 'react'

export const TopRight: React.FC = () => {
  return (
    <Flex direction="column">
      <Heading as="h6" size="sm" mt={4}>
        BNB/BUSD
      </Heading>
      <Heading as="h5" size="lg" color="teal" mt={2}>
        632.52
      </Heading>
      <HStack fontSize="sm" mt={2}>
        <Text color="#606060">$631.88</Text>
        <Text color="teal">+1.19%</Text>
      </HStack>
    </Flex>
  )
}
