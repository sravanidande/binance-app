import { Box, Flex } from '@chakra-ui/layout'
import React from 'react'
import { TopLeft } from '../../components/column-1'
import { TopRight } from '../../components/column-1'
import { TabView } from '../../components/column-1'

export const TopView: React.FC = () => {
  return (
    <Box bg="#171923" p={4}>
      <TabView />
      <Flex justifyContent="space-between">
        <TopRight />
        <TopLeft />
      </Flex>
    </Box>
  )
}
