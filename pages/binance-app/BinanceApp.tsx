import React from 'react'
import { TopView } from '../../components/column-1'
import { MiddleView } from '../../components/column-2'
import { FooterView } from '../../components/column-3'

const BinanceApp: React.FC = () => {
  return (
    <>
      <TopView />
      <MiddleView />
      <FooterView />
    </>
  )
}

export default BinanceApp
